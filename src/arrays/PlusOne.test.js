import React from 'react'
import { render, fireEvent, waitForElement, getByDisplayValue } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import PlusOne from './PlusOne'

test('adds one to a number other than 9', async ()=> {
    // given
    const array = [1]
    const {getByText, getByRole} = render(<PlusOne array={array} />)

    // when
    fireEvent.click(getByText('PlusOne'))

    // then
    expect(getByRole('heading').textContent).toBe('[2]')
})

test('adds one to a single 9', async ()=> {
    // given
    const array = [9]
    const {getByText, getByRole} = render(<PlusOne array={array} />)

    // when
    fireEvent.click(getByText('PlusOne'))

    // then
    expect(getByRole('heading').textContent).toBe('[1,0]')
})

test('adds one to a array of two with one 9', async ()=> {
    // given
    const array = [1,9]
    const {getByText, getByRole} = render(<PlusOne array={array} />)

    // when
    fireEvent.click(getByText('PlusOne'))

    // then
    expect(getByRole('heading').textContent).toBe('[2,0]')
})

test('adds one to a array of two 9s', async ()=> {
    // given
    const array = [9,9]
    const {getByText, getByRole} = render(<PlusOne array={array} />)

    // when
    fireEvent.click(getByText('PlusOne'))

    // then
    expect(getByRole('heading').textContent).toBe('[1,0,0]')
})