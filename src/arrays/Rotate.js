import React, {useState} from 'react';

export default function Rotate({args}) {
    const [rotatedArray, setRotatedArray] = useState('')

    function rotate() {
        let array = args.array
        let step = args.steps % array.length
        let values = []

        for(let i = 0; i < step; i++) {
            values.push(array[i])
            array[i] = array[array.length - step + i]
        }

        for(let i = step; i < array.length; i++) {
            values.push(array[i])
            array[i] = values.shift()
        }

        setRotatedArray(array.toString())
    }

    return (
        <div>
            <button onClick={rotate}>Rotate</button>
            <h1>[{rotatedArray}]</h1>
        </div>
    )
}