import React, { useState } from 'react'

export default function PlusOne({array}) {
    const [result, setResult] = useState('')

    function justOne(i) {
        if (array[i] != 9) {
            array[i] += 1
        } else {
            array[i] = 0
            if (i == 0) {
                array[i] = 1
                array.push(0)
                return;
            }
            justOne(i - 1)
        }
    }

    function plusOne() {
        justOne(array.length - 1)       

        setResult(array.toString())
    }

    return(<div>
        <button onClick={plusOne} >PlusOne</button>
        <h1>[{result}]</h1>
    </div>)
}