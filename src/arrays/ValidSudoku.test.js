import React from 'react'
import { render, fireEvent, screen } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import ValidSudoku from './ValidSudoku'

// left commented out for examples of what I started with
// test('a one-d array is Valid with no duplicates', () => {
//     // given
//     const array = [
//         ["1","3",".","7"]
//     ]
//     render(<ValidSudoku array={array} />)

//     // when
//     fireEvent.click(screen.getByText('Valid Sudoku'))

//     // then
//     expect(getByRole('heading').textContent).toBe('Valid')
// })

// test('a two-d array is InValid with duplicates', () => {
//     // given
//     const array = [
//         ["1","3","","7"],
//         ["2","3","","8"],
//     ]
//     render(<ValidSudoku array={array} />)

//     // when
//     fireEvent.click(screen.getByText('Valid Sudoku'))

//     // then
//     expect(getByRole('heading').textContent).toBe('InValid')
// })

test('a array is Valid with no duplicates', () => {
    // given
    const array = [
        ["5","3",".",".","7",".",".",".","."],
        ["6",".",".","1","9","5",".",".","."],
        [".","9","8",".",".",".",".","6","."],
        ["8",".",".",".","6",".",".",".","3"],
        ["4",".",".","8",".","3",".",".","1"],
        ["7",".",".",".","2",".",".",".","6"],
        [".","6",".",".",".",".","2","8","."],
        [".",".",".","4","1","9",".",".","5"],
        [".",".",".",".","8",".",".","7","9"]
      ]
    render(<ValidSudoku array={array} />)

    // when
    fireEvent.click(screen.getByText('Valid Sudoku'))

    // then
    expect(screen.getByRole('heading').textContent).toBe('Valid')
})

test('a array is InValid with duplicates in the row', () => {
    // given
    const array = [
        ["5","3",".",".","7",".",".",".","."],
        ["6",".",".","1","9","5",".",".","."],
        [".","9","8",".",".",".",".","9","."],
        ["8",".",".",".","6",".",".",".","3"],
        ["4",".",".","8",".","3",".",".","1"],
        ["7",".",".",".","2",".",".",".","6"],
        [".","6",".",".",".",".","2","8","."],
        [".",".",".","4","1","9",".",".","5"],
        [".",".",".",".","8",".",".","7","9"]
      ]
    render(<ValidSudoku array={array} />)

    // when
    fireEvent.click(screen.getByText('Valid Sudoku'))

    // then
    expect(screen.getByRole('heading').textContent).toBe('InValid')
})

test('a array is InValid with duplicates in the col', () => {
    // given
    const array = [
        ["5","3",".",".","7",".",".",".","."],
        ["6",".",".","1","9","5",".",".","."],
        [".","9","8",".",".",".",".","6","."],
        ["8",".",".",".","6",".",".",".","3"],
        ["4",".",".","8",".","3",".",".","1"],
        ["7",".",".",".","2",".",".",".","6"],
        [".","6",".",".",".",".","2","8","."],
        [".",".",".","4","1","9",".",".","5"],
        ["6",".",".",".","8",".",".","7","9"]
      ]
    render(<ValidSudoku array={array} />)

    // when
    fireEvent.click(screen.getByText('Valid Sudoku'))

    // then
    expect(screen.getByRole('heading').textContent).toBe('InValid')
})

test('a array is InValid with duplicates in 3x3', () => {
    // given
    const array = [
        ["9","3",".",".","7",".",".",".","."],
        ["6",".",".","1","9","5",".",".","."],
        [".","9","8",".",".",".",".","6","."],
        ["8",".",".",".","6",".",".",".","3"],
        ["4",".",".","8",".","3",".",".","1"],
        ["7",".",".",".","2",".",".",".","6"],
        [".","6",".",".",".",".","2","8","."],
        [".",".",".","4","1","9",".",".","5"],
        [".",".",".",".","8",".",".","7","9"]
      ]
    render(<ValidSudoku array={array} />)

    // when
    fireEvent.click(screen.getByText('Valid Sudoku'))

    // then
    expect(screen.getByRole('heading').textContent).toBe('InValid')
})

test('a array is InValid with duplicates in a different 3x3', () => {
    // given
    const array = [
        ["5","3",".",".","7",".",".",".","."],
        ["6",".",".","1","9","5",".",".","."],
        [".","9","8",".","5",".",".","6","."],
        ["8",".",".",".","6",".",".",".","3"],
        ["4",".",".","8",".","3",".",".","1"],
        ["7",".",".",".","2",".",".",".","6"],
        [".","6",".",".",".",".","2","8","."],
        [".",".",".","4","1","9",".",".","5"],
        [".",".",".",".","8",".",".","7","9"]
      ]
    render(<ValidSudoku array={array} />)

    // when
    fireEvent.click(screen.getByText('Valid Sudoku'))

    // then
    expect(screen.getByRole('heading').textContent).toBe('InValid')
})

test('a array is InValid with duplicates in a different 3x3 but lower', () => {
    // given
    const array = [
        ["5","3",".",".","7",".",".",".","."],
        ["6",".",".","1","9","5",".",".","."],
        [".","9","8",".",".",".",".","6","."],
        ["8",".",".",".","6",".",".",".","3"],
        ["4",".",".","8",".","3",".",".","1"],
        ["7",".",".",".","2",".",".",".","6"],
        [".","6",".",".",".",".","5","8","."],
        [".",".",".","4","1","9",".",".","5"],
        [".",".",".",".","8",".",".","7","9"]
      ]
    render(<ValidSudoku array={array} />)

    // when
    fireEvent.click(screen.getByText('Valid Sudoku'))

    // then
    expect(screen.getByRole('heading').textContent).toBe('InValid')
})
