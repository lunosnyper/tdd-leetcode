import React, { useState } from 'react';

export default function RemoveDups({array}) {
    const [arrayNoDupsText, setArrayText] = useState('')
    const [size, setSize] = useState('')
    
    function removeDups(){
        let size = 1;
        let index = 0;
        for(let i=0; i < array.length; i++) {
            if (array[index] !== array[i]) {
                index++;
                array[index] = array[i];
                size++;
            }
        }
        setArrayText(array.toString())
        setSize(size)
    }

    

    return (
        <div>
            <button onClick={removeDups}>Remove Dups</button>
            <h1>[{arrayNoDupsText}]</h1>
            <p id='size'>{size}</p>
        </div>
    )
}