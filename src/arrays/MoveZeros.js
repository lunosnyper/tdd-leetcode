import React, { useState } from 'react'

export default function MoveZeros({array}) {
    const [arrayString, setArrayString] = useState('')

    function moveZeros() {
        let lastIndex = 0
        for(let i = 0; i < array.length; i++) {
            if(array[i] !== 0) {
                let temp = array[lastIndex]
                array[lastIndex++] = array[i]
                array[i] = temp
            }
         }
        setArrayString(array.toString())
    }

    return(<div>
        <button onClick={moveZeros}>Move Zeros</button>
        <h1>[{arrayString}]</h1>
    </div>)
}