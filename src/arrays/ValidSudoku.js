import React, { useState } from 'react'

export default function ValidSudoku({array}) {
    const [valid, setValid] = useState('')

    function validSudoku() {
        const rowMap = new Map()
        const colMap = new Map()
        const threeMap = new Map()
        let valid = 'Valid'

        for(let j = 0; j < array.length; j++) {
            let rowArray = array[j]
            rowMap.clear()
            colMap.clear()
            threeMap.clear()
            let jThreeColMod = Math.floor(j / 3) * 3
            let jThreeRowMod = j % 3 * 3
            for(let i = 0; i < rowArray.length; i++) {
                let rowValue = rowArray[i]
                let colValue = array[i][j]
                let threeValue = array[Math.floor(i / 3) + jThreeColMod][i % 3 + jThreeRowMod]

                if(rowMap.has(rowValue) 
                || colMap.has(colValue)
                || threeMap.has(threeValue)
                ) {
                    valid = 'InValid'
                    break
                } else {
                    if (rowValue !== '.')
                        rowMap.set(rowValue, i)
                    if (colValue !== '.')
                        colMap.set(colValue, i)
                    if (threeValue !== '.')
                        threeMap.set(threeValue, i)
                }
            }
        }

        setValid(valid)
    }

    return(
        <div>
            <button onClick={validSudoku}>Valid Sudoku</button>
            <h1>{valid}</h1>
        </div>
    )
}