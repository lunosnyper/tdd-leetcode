import React from 'react'
import { render, fireEvent, screen } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import MoveZeros from './MoveZeros'

test('moves one zero', () => {
    // given
    const array = [0,1]
    render(<MoveZeros array={array} />)

    // when
    fireEvent.click(screen.getByText('Move Zeros'))

    // then
    expect(screen.getByRole('heading').textContent).toBe('[1,0]')
})

test('moves a zero with bigger array', () => {
    // given
    const array = [0,1,2,3]
    render(<MoveZeros array={array} />)

    // when
    fireEvent.click(screen.getByText('Move Zeros'))

    // then
    expect(screen.getByRole('heading').textContent).toBe('[1,2,3,0]')
})

test('moves two zeros with bigger array', () => {
    // given
    const array = [0,1,0,2,3]
    render(<MoveZeros array={array} />)

    // when
    fireEvent.click(screen.getByText('Move Zeros'))

    // then
    expect(screen.getByRole('heading').textContent).toBe('[1,2,3,0,0]')
})

test('moves a zero a number and a zero with bigger array', () => {
    // given
    const array = [0,1,0]
    render(<MoveZeros array={array} />)

    // when
    fireEvent.click(screen.getByText('Move Zeros'))

    // then
    expect(screen.getByRole('heading').textContent).toBe('[1,0,0]')
})

test('moves a zero a zero and a number  with bigger array', () => {
    // given
    const array = [0,0,1]
    render(<MoveZeros array={array} />)

    // when
    fireEvent.click(screen.getByText('Move Zeros'))

    // then
    expect(screen.getByRole('heading').textContent).toBe('[1,0,0]')
})