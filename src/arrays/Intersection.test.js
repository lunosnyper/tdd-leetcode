import React from 'react'
import { render, fireEvent, waitForElement, getByDisplayValue } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import Intersection from './Intersection'

test('takes a two digit array and three digit array with two matching numbers and returns the array of matching numbers', async () => {
    // given
    const arrays = {
        first: [2,2],
        second: [1,2,2]
    }
    const {getByRole, getByText} = render(<Intersection arrays={arrays} />)

    // when
    fireEvent.click(getByText('Intersection'))

    // then
    expect(getByRole('heading').textContent).toBe('[2,2]')
})

test('given a different number of the same number in the first array than the second', async () => {
    // given
    const arrays = {
        first: [2,2],
        second: [1,2,2,2]
    }
    const {getByRole, getByText} = render(<Intersection arrays={arrays} />)

    // when
    fireEvent.click(getByText('Intersection'))

    // then
    expect(getByRole('heading').textContent).toBe('[2,2]')
})