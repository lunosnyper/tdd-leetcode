import React from 'react'
import { render, fireEvent, screen} from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import BuySellStockMaxProfit from './BuySellStock'

test('can calculate max profit from a two instance array', async () => {
    // given
    const array = [1,2]
    render(<BuySellStockMaxProfit array={array} />)

    // when
    fireEvent.click(screen.getByText('Max Profit'))

    // then
    expect(screen.getByRole('heading').textContent).toBe('1')
})

test('can calculate max profit from a larger instance array', async () => {
    // given
    const array = [1,2,1,3]
    render(<BuySellStockMaxProfit array={array} />)

    // when
    fireEvent.click(screen.getByText('Max Profit'))

    // then
    expect(screen.getByRole('heading').textContent).toBe('3')
})

test('can calculate max profit from a no profit array', async () => {
    // given
    const array = [7,6,4,3,1]
    render(<BuySellStockMaxProfit array={array} />)

    // when
    fireEvent.click(screen.getByText('Max Profit'))

    // then
    expect(screen.getByRole('heading').textContent).toBe('0')
})

test('can calculate max profit from a up and down instance array', async () => {
    // given
    const array = [7,1,5,3,6,4]
    render(<BuySellStockMaxProfit array={array} />)

    // when
    fireEvent.click(screen.getByText('Max Profit'))

    // then
    expect(screen.getByRole('heading').textContent).toBe('7')
})