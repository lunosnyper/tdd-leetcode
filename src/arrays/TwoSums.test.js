import React from 'react'
import { render, fireEvent, waitForElement, getByDisplayValue } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import TwoSums from './TwoSums'

test('adds the first and the second', () => {
    // given
    const args = {
        array:[1,2],
        target: 3
    }
    const {getByText, getByRole} = render(<TwoSums args={args} />)

    // when
    fireEvent.click(getByText('Two Sums'))

    // then
    expect(getByRole('heading').textContent).toBe('[0,1]')
})

test('adds the first and finds the second', () => {
    // given
    const args = {
        array:[1,0,2],
        target: 3
    }
    const {getByText, getByRole} = render(<TwoSums args={args} />)

    // when
    fireEvent.click(getByText('Two Sums'))

    // then
    expect(getByRole('heading').textContent).toBe('[0,2]')
})

test('adds the found first and finds the second', () => {
    // given
    const args = {
        array:[0,1,2],
        target: 3
    }
    const {getByText, getByRole} = render(<TwoSums args={args} />)

    // when
    fireEvent.click(getByText('Two Sums'))

    // then
    expect(getByRole('heading').textContent).toBe('[1,2]')
})

test('first and finds the second from bigger array', () => {
    // given
    const args = {
        array:[2,7,11,15],
        target: 9
    }
    const {getByText, getByRole} = render(<TwoSums args={args} />)

    // when
    fireEvent.click(getByText('Two Sums'))

    // then
    expect(getByRole('heading').textContent).toBe('[0,1]')
})