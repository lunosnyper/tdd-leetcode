import React from 'react'
import { render, fireEvent, screen} from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import RemoveDups from './RemoveDups'

test('removes duplicate from given array when duplicates are first two numbers', async () => {
    // given
    const array = [1,1,2]
    render(<RemoveDups array={array} />)

    // when
    fireEvent.click(screen.getByText('Remove Dups'))

    // then
    await screen.findByRole('heading')

    expect(screen.getByRole('heading').textContent).toBe('[1,2,2]')
    expect(screen.getByText('2')).toBeInTheDocument()
})

test('removes duplicate from given array when duplicates anywhere in array', async () => {
    // given
    const array = [1,2,3,3,4]
    render(<RemoveDups array={array} />)

    // when
    fireEvent.click(screen.getByText('Remove Dups'))

    // then
    await screen.findByRole('heading')

    expect(screen.getByRole('heading')).toHaveTextContent('[1,2,3,4,4]')
    expect(screen.getByText('4')).toBeInTheDocument()
})

test('removes duplicate from given array when duplicates anywhere in array but bigger', async () => {
    // given
    const array = [0,0,1,1,1,2,2,3,3,4]
    render(<RemoveDups array={array} />)

    // when
    fireEvent.click(screen.getByText('Remove Dups'))

    // then
    await screen.findByRole('heading')

    expect(screen.getByRole('heading')).toHaveTextContent('[0,1,2,3,4,2,2,3,3,4]')
    expect(screen.getByText('5')).toBeInTheDocument()
})