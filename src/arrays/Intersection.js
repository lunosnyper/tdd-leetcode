import React, { useState } from 'react';

export default function Intersection({arrays}) {
    const [intersectionArray, setIntersectionArray] = useState('')

    function intersection() {
        let array = []
        let firstMap = new Map()
        let first = arrays.first;
        let second = arrays.second;

        for(let i = 0; i < first.length; i++) {
            let temp = first[i]
            if (!firstMap.has(temp)) {
                firstMap.set(temp, 1)
            } else {
                firstMap.set(temp, firstMap.get(temp) + 1)
            }
        }

        for(let i = 0; i < second.length; i++) {
            let temp = second[i]
            if(firstMap.has(temp) && firstMap.get(temp) > 0) {
                firstMap.set(temp, firstMap.get(temp) - 1)
                array.push(second[i])
            }
        }

        setIntersectionArray(array.toString())
    }

    return(<div>
        <button onClick={intersection}>Intersection</button>
        <h1>[{intersectionArray}]</h1>
    </div>)
}