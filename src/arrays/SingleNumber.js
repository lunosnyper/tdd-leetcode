import React, { useState } from 'react';

export default function SingleNumber({array}) {
    const [theNumber, setTheNumber] = useState('')

    function singleNumber() {
        let number = 0
        for(let i = 0; i < array.length; i++) {
            number ^= array[i]
        }
        setTheNumber(number)
    }

    return(
        <div>
            <button onClick={singleNumber}>Single Number</button>
            <h1>{theNumber}</h1>
        </div>
    )
}