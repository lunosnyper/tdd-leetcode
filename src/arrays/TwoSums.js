import React, { useState } from 'react'

export default function TwoSums({args}) {
    const [indecies, setIndecies] = useState('')

    function twoSums() {
        let array = args.array
        let solution = []
        let arrayMap = new Map()

        for(let i = 0; i < array.length; i++) {
            let difference = args.target - array[i]
            if (arrayMap.has(difference)) {
                solution[0] = arrayMap.get(difference)
                solution[1] = i
                break
            } else {
                arrayMap.set(array[i], i)
            }
        }

        setIndecies(solution.toString())
    }

    return (
        <div>
            <button onClick={twoSums}>Two Sums</button>
            <h1>[{indecies}]</h1>
        </div>
    )
}