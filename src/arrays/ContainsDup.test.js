import React from 'react'
import { render, fireevent, fireEvent, getByText } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import ContainsDup from './ContainsDup'

test('array of two of the same numbers returns true', async () => {
    // given
    const array = [1,1]
    const { getByText, getByRole } = render(<ContainsDup array={array} />)

    // when
    fireEvent.click(getByText('Contains Duplication'))

    // then
    expect(getByRole('heading').textContent).toBe('true')
})

test('array of two different numbers returns false', async () => {
    // given
    const array = [1,2]
    const { getByText, getByRole } = render(<ContainsDup array={array} />)

    // when
    fireEvent.click(getByText('Contains Duplication'))

    // then
    expect(getByRole('heading').textContent).toBe('false')
})

test('large array of some duplicate numbers returns true', async () => {
    // given
    const array = [1,1,1,3,3,4,3,2,4,2]
    const { getByText, getByRole } = render(<ContainsDup array={array} />)

    // when
    fireEvent.click(getByText('Contains Duplication'))

    // then
    expect(getByRole('heading').textContent).toBe('true')
})

test('large array of no duplicate numbers returns false', async () => {
    // given
    const array = [1,2,3,4,5,6,7,8,9]
    const { getByText, getByRole } = render(<ContainsDup array={array} />)

    // when
    fireEvent.click(getByText('Contains Duplication'))

    // then
    expect(getByRole('heading').textContent).toBe('false')
})