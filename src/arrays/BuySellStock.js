import React, { useState } from 'react';

export default function BuySellStockMaxProfit({array}) {
    const [profit, setProfit] = useState('')

    function maxProfit() {
        let profit = 0;

        for (let i = 0; i < array.length - 1; i++) {
            if (array[i] < array[i+1]) {
                profit += array[i+1] - array[i];
            }
        }

        setProfit(profit)
    }

    return (
        <div>
            <button onClick={maxProfit}>Max Profit</button>
            <h1>{profit}</h1>
        </div>
    )
}