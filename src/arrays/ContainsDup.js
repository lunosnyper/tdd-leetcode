import React, { useState } from 'react';

export default function ContainsDup({array}) {
    const [hasDups, setHasDups] = useState('')

    function hasDuplicates() {
        array.sort()

        for(let i = 1; i < array.length; i++) {
            if(array[i-1] === array[i]) {
                setHasDups('true')
                return;
            }
        }
        setHasDups('false')
    }

    return(
        <div>
            <button onClick={hasDuplicates}>Contains Duplication</button>
            <h1>{hasDups}</h1>
        </div>
    )
}