import React from 'react'
import { render, fireEvent, screen} from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import SingleNumber from './SingleNumber'

test('given an array with two of the same numbers and one different returns different number', async () => {
    // given
    const array = [2,2,1]
    render(<SingleNumber array={array} />)

    // when
    fireEvent.click(screen.getByText('Single Number'))

    // then
    expect(screen.getByRole('heading').textContent).toBe('1')
})

test('given a large array with duplicate numbers and one different returns different number', async () => {
    // given
    const array = [4,1,2,1,2]
    render(<SingleNumber array={array} />)

    // when
    fireEvent.click(screen.getByText('Single Number'))

    // then
    expect(screen.getByRole('heading').textContent).toBe('4')
})