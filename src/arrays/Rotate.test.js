import React from 'react'
import { render, fireEvent, waitForElement, getByDisplayValue } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import Rotate from './Rotate'

test('handles steps that are bigger than the single element array', async () => {
    // given
    const args = {
        array:[-1],
        steps:2
    }
    const { getByText, getByRole } = render(<Rotate args={args} />)

    // when
    fireEvent.click(getByText('Rotate'))

    // then
    expect(getByRole('heading').textContent).toBe('[-1]')
})



test('can switch one number in a two instance array', async () => {
    // given
    const args = {
        array:[1,2],
        steps:1
    }
    const { getByText, getByRole } = render(<Rotate args={args} />)

    // when
    fireEvent.click(getByText('Rotate'))

    // then
    expect(getByRole('heading').textContent).toBe('[2,1]')
})

test('handles steps that are bigger than the array', async () => {
    // given
    const args = {
        array:[1,2],
        steps:3
    }
    const { getByText, getByRole } = render(<Rotate args={args} />)

    // when
    fireEvent.click(getByText('Rotate'))

    // then
    expect(getByRole('heading').textContent).toBe('[2,1]')
})

test('can step 2 times in a larger instance array', async () => {
    // given
    const args = {
        array:[1,2,3,4],
        steps:2
    }
    const { getByText, getByRole } = render(<Rotate args={args} />)

    // when
    fireEvent.click(getByText('Rotate'))

    // then
    expect(getByRole('heading').textContent).toBe('[3,4,1,2]')
})

test('can step 3 in a larger instance array', async () => {
    // given
    const args = {
        array:[1,2,3,4,5,6,7],
        steps:3
    }
    const { getByText, getByRole } = render(<Rotate args={args} />)

    // when
    fireEvent.click(getByText('Rotate'))

    // then
    expect(getByRole('heading').textContent).toBe('[5,6,7,1,2,3,4]')
})