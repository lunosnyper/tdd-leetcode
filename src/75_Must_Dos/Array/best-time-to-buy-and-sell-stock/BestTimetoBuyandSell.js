/*
You are given an array prices where prices[i] is the price of a given stock on the ith day.

You want to maximize your profit by choosing a single day to buy one stock and choosing a different day in the future to sell that stock.

Return the maximum profit you can achieve from this transaction. If you cannot achieve any profit, return 0.
*/

export class BestTimetoBuyandSell {
  constructor(prices) {
    this.prices = prices;
  }

  // Brute Forcing it
  
  // getProfit() {
  //   let mostProfit = 0;
  //   for(let i = 0; i < this.prices.length; i++) {
  //     for(let j = i + 1; j < this.prices.length; j++) {
  //       if(this.prices[j] - this.prices[i] > mostProfit)
  //         mostProfit = this.prices[j] - this.prices[i]
  //     }
  //   }
  //   return mostProfit;  
  // }

  getProfit() {
    return 0;
  }
}