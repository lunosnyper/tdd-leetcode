import { BestTimetoBuyandSell } from "./BestTimetoBuyandSell";

describe("BestTimetoBuyandSell", () => {
  test("given an array with prices 1 and 2 return 1", () => {
    // given
    const prices = [1, 2];

    // when
    const bestBuyandSell = new BestTimetoBuyandSell(prices);

    // then
    expect(bestBuyandSell.getProfit()).toBe(1);
  });

  test("given an array with more prices returns profit", () => {
    // given
    const prices = [7,1,5];

    // when
    const bestBuyandSell = new BestTimetoBuyandSell(prices);

    // then
    expect(bestBuyandSell.getProfit()).toBe(4);
  });

  test("given an array with two dips in prices returns profit", () => {
    // given
    const prices = [7,1,5,3,6,4];

    // when
    const bestBuyandSell = new BestTimetoBuyandSell(prices);

    // then
    expect(bestBuyandSell.getProfit()).toBe(5);
  });

  test("given an array with no profit return 0", () => {
    // given
    const prices = [7,6,5,4,3,2,1];

    // when
    const bestBuyandSell = new BestTimetoBuyandSell(prices);

    // then
    expect(bestBuyandSell.getProfit()).toBe(0);
  });
});
